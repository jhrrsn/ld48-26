window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame       ||
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame    ||
          function( callback ){
            window.setTimeout(callback, 1000 / 60);
          };
})();
var cancelAnimationFrame = window.cancelAnimationFrame || window.mozCancelRequestAnimationFrame || window.webkitCancelRequestAnimationFrame || window.msCancelRequestAnimationFrame;
window.cancelAnimationFrame = cancelAnimationFrame;
var level = 1;

var bmusic = document.getElementById("bmusic");
var wmusic = document.getElementById("win");
var jumpSound = document.getElementById("jump");
jumpSound.volume=0.8;
var ghostSound = document.getElementById("ghost");
ghostSound.volume=0.8;
var deathSound = document.getElementById("death");
var landSound = document.getElementById("land");
landSound.volume=0.2;

var foreground = document.getElementById("foreground");
var ctx = foreground.getContext("2d");
var background = document.getElementById("background");
var ctxB = background.getContext("2d");
var credits = new Image();
credits.src = 'img/meta/credits.png';
var width = 1024;
var height = 640;
foreground.width = width;
foreground.height = height;
background.width = width;
background.height = height;
var keys = [];
var fallen = false;
var ghostKill = false;

function canvasApp() {

  ///////////////////////
  // Initial Variables //
  ///////////////////////

  ctx.clearRect(0, 0, width, height);
  var friction = 0.8;
  var gravity = 0.4;
  var currentLevel = level;
  var land, platforms, enemies, player;
  var levelColours = [];
  var killCount = 0;

  if (level <= 5) {
    $.ajax({
      url: "data/levels.json",
      async: false,
      dataType: 'json',
      success: function (json) {
        var l = 'l' + currentLevel;
        land = json[l]['Land'];
        platforms = json[l]['Platforms'];
        enemies = json[l]['Enemies'];
        player = json[l]['Player'];
        for (var i = 0; i < enemies.length; i++) {
          levelColours.push(enemies[i].colour);
          enemies[i]['float'] = 0;
          enemies[i]['up'] = true;
          enemies[i]['range'] = 0;
          enemies[i]['left'] = true;
        }
      }
    });
    var ghostsL = {};
    for (var i = 0; i < levelColours.length; i++) {
      var fileref = 'img/ghosts/ghost' + levelColours[i] + 'L.png'
      var ghost = new Image();
      ghost.src = fileref;
      var colour = levelColours[i];
      ghostsL[colour] = ghost;
    };

    var ghostsR = {};
    for (var i = 0; i < levelColours.length; i++) {
      var fileref = 'img/ghosts/ghost' + levelColours[i] + 'R.png'
      var ghost = new Image();
      ghost.src = fileref;
      var colour = levelColours[i];
      ghostsR[colour] = ghost;
    };
    var aColours = [];
    var playerParticles = [];
    var explosions = [];
    var explosionParticles = [];
    var positions = ['stand', 'jump', 'fall', 'run0', 'run1'];
    var running = 0;
    var playerL = [];
    for (var i = 0; i < positions.length; i++) {
      sprite = new Image();
      sprite.src = 'img/player/left/' + positions[i] + '.png';
      playerL.push(sprite); 
    }
    var playerR = [];
    for (var i = 0; i < positions.length; i++) {
      sprite = new Image();
      sprite.src = 'img/player/right/' + positions[i] + '.png';
      playerR.push(sprite); 
    }

    fallen = false;
    ghostKill = false;

    var ghostDeath = new Image();
    ghostDeath.src = 'img/meta/ghostDeath.png'

    var fallDeath = new Image();
    fallDeath.src = 'img/meta/fallDeath.png'

    
    var bkgd = new Image();
    bkgd.src = 'img/level/level' + currentLevel + '.png';
    bkgd.onload = function() {
      ctxB.drawImage(bkgd, 0, 0, width, height);
      (function animloop(){
        loop = requestAnimFrame(animloop);
        gameLoop();
        draw();
      })();
    };
  }

  if (level > 5) {
    ctx.drawImage(credits, 0, 0);
  }

  


  ///////////////
  // Game Loop //
  ///////////////

  function gameLoop() {

    if (keys[32]) {
      cancelAnimationFrame(loop);
      canvasApp();
    }

    if (player.y > height) {
      if (player.alive) {
        death.play();
        fallen = true;
        player.alive = false;
      }
    } 
    else if ((level != 3 && level != 4 && level != 5 && player.y > player.winY && player.x > player.winX && !player.win) 
      || ((level === 3 || level === 4 || level === 5) && player.y < player.winY && player.x > player.winX && !player.win)) {
      level += 1;
      if (level === 6) {
        bmusic.pause();
        wmusic.play();
      }
      ctx.clearRect(0, 0, width, height);
      ctxB.clearRect(0, 0, width, height);
      cancelAnimationFrame(loop);
      canvasApp();
    }

    if (player.alive && !player.win) {
      if (keys[38]) {
        if (!player.j && player.g) {
          jumpSound.play();
          player.j = true;
          player.vY = -player.s*1.5;
        }
      }

      if (keys[39]) {
        if (player.vX < player.s) {
          player.vX += 0.7;
        }
      }

      if (keys[37]) {
        if (player.vX > -player.s) {
          player.vX -= 0.7;
        }
      }

      player.vX *= friction;
      player.vY += gravity;
      player.g = false;

      for (var i = 0; i < land.length; i++) {
        var dir = colCheck(player, land[i]);
        if (dir === "l" || dir === "r") {
          player.vX = 0;
        } else if (dir === "b") {
          if (player.j) {
            landSound.play();
          }
          player.g = true;
          player.j = false;
        } else if (dir === "t") {
          player.vY = 0;
        }
      }

      for (var i = 0; i < platforms.length; i++) {
        if (aColours.indexOf(platforms[i].colour) >= 0) {
          var dir = colCheck(player, platforms[i]);
          if (dir === "b") {
            if (player.p) {
              player.p = false;
            } else {
              if (player.j) {
                landSound.play();
              }
              player.g = true;
              player.j = false;
            }
          } else if (dir === "t") {
            player.p = true;
          }
        }
      }

      for (var i = 0; i < enemies.length; i++) {
        if (enemies[i].alive) {
          // Float (up/down)
          if (enemies[i].up) {
            enemies[i].float -= 0.2;
            enemies[i].y -= 0.2;
            if (Math.abs(enemies[i].float) > 4) {
              enemies[i].up = false;
            }
          } else {
            enemies[i].float += 0.2;
            enemies[i].y += 0.2;
            if (Math.abs(enemies[i].float) > 4) {
              enemies[i].up = true;
            }
          }
          // Wander (left/right)
          if (enemies[i].left) {
            enemies[i].range -= 0.2;
            enemies[i].x -= 0.2;
            if (Math.abs(enemies[i].range) > enemies[i].rangeMax) {
              enemies[i].left = false;
            }
          } else {
            enemies[i].range += 0.2;
            enemies[i].x += 0.2;
            if (Math.abs(enemies[i].range) > enemies[i].rangeMax) {
              enemies[i].left = true;
            }
          }
          
          var dir = colCheck(player, enemies[i]);
          if (dir === "b") {
            landSound.play();
            ghostSound.play();
            killCount += 1;
            enemies[i].alive = false;
            explosions.push({
              x : enemies[i].x+15,
              y : enemies[i].y+30,
              colour : enemies[i].colour,
              decay : 5
            })
            aColours.push(enemies[i].colour);
            player.j = true;
            player.vY = -player.s*1.2;
          } else if (dir === "l" || dir === "r") {
            death.play();
            ghostKill = true;
            player.alive = false;
          } 
        }
      }

      if (aColours.indexOf("red") >= 0 && aColours.indexOf("blue") >= 0 && aColours.indexOf("purple") < 0) {
        aColours.push('purple');
      } else if (aColours.indexOf("red") >= 0 && aColours.indexOf("yellow") >= 0 && aColours.indexOf("orange") < 0) {
        aColours.push('orange');
      } else if (aColours.indexOf("yellow") >= 0 && aColours.indexOf("blue") >= 0 && aColours.indexOf("green") < 0) {
        aColours.push('green');
      } else if (aColours.indexOf("red") >= 0 && aColours.indexOf("blue") >= 0 && aColours.indexOf("yellow") >= 0 && aColours.indexOf("white") < 0 && killCount >= 6) {
        aColours.push('white');
      }
      
      if(player.g){
        player.vY = 0;
      }

      if (aColours.length > 0 && explosions.length <= 0 && player.alive) {
        var particle = {
          x : player.x,
          y : player.y,
          xSpeed: Math.random() * (0.1 + 0.1) -0.1,
          ySpeed: Math.random() * (0 + 1) - 1,
          colour : Math.floor(Math.random() * aColours.length), 
          size : 3,
          opacity : 1,
          alive : true
        }
        playerParticles.push(particle);

        if (level === 5) {
          var particle = {
            x : 588 + Math.random() * 305,
            y : 86,
            xSpeed: Math.random() * (0.1 + 0.1) -0.1,
            ySpeed: Math.random() * (0 + 1) - 1,
            colour : Math.floor(Math.random() * aColours.length), 
            size : 3,
            opacity : 1,
            alive : true
          }
          playerParticles.push(particle);
        }
      }

      if (explosions.length > 0) {
        for (var i = 0; i < explosions.length; i++) {
          if (explosions[i].decay < 0.1) {
            explosions.splice(i, 1);
            i++;
          }
          var particle = {
            x : explosions[i].x + Math.random() * (5 + 5) - 5,
            y : explosions[i].y + Math.random() * (5 + 5) - 5,
            xSpeed: Math.random() * (2 + 2) - 2,
            ySpeed: Math.random() * (0 + 2) - 2,
            colour : explosions[i].colour, 
            size : 4,
            opacity : 1,
            alive : true
          }
          explosionParticles.push(particle);
          var particle = {
            x : explosions[i].x + Math.random() * (5 + 5) - 5,
            y : explosions[i].y + Math.random() * (5 + 5) - 5,
            xSpeed: Math.random() * (2 + 2) - 2,
            ySpeed: Math.random() * (0 + 2) - 2,
            colour : explosions[i].colour, 
            size : 4,
            opacity : 1,
            alive : true
          }
          explosionParticles.push(particle);
          explosions[i].decay -= 0.4;
        }
      }

      player.x += player.vX;
      player.y += player.vY;
    }
  };


  //////////
  // Draw //
  //////////

  function draw() {
    if (level <= 5) {
      ctx.globalAlpha = 1;
      ctx.clearRect(0, 0, width, height);
      for (var i = 0; i < enemies.length; i++) {
        if (enemies[i].alive) {
          var ghost;
          if (enemies[i].left) {
            ghost = ghostsL[enemies[i].colour];
          } else {
            ghost = ghostsR[enemies[i].colour];
          }
          ctx.drawImage(ghost,enemies[i].x, enemies[i].y);
        }
      }
      for (var i = 0; i < platforms.length; i++) {
        if (aColours.indexOf(platforms[i].colour) >= 0) {
          if (platforms[i].o < 0.6) {
            platforms[i].o += 0.03;
          }
          ctx.globalAlpha = platforms[i].o;
          ctx.fillStyle = getColour(platforms[i].colour);
          ctx.fillRect(platforms[i].x, platforms[i].y, platforms[i].w, platforms[i].h);
        }
      }
      

      if (aColours.length > 0 && player.alive) {
        for (var i = 0; i < playerParticles.length; i++) {
          p = playerParticles[i];
          if (p.opacity < 0.1) {
            p.alive = false;
          }
          if (p.alive) {
            ctx.fillStyle = getColour(aColours[p.colour]);
            ctx.globalAlpha = p.opacity;
            ctx.fillRect(p.x, p.y, p.size, p.size);
            p.opacity -= 0.01;
            p.x = p.x + p.xSpeed;
            p.y = p.y + p.ySpeed;
            p.size *= 0.99
          } else {
            playerParticles.splice(i, 1);
            i++;
          }
        }
      }

      if (explosionParticles) {
        for (var i = 0; i < explosionParticles.length; i++) {
          p = explosionParticles[i];
          if (p.opacity < 0.1) {
            p.alive = false;
          }
          if (p.alive) {
            ctx.fillStyle = getColour(p.colour);
            ctx.globalAlpha = p.opacity;
            ctx.fillRect(p.x, p.y, p.size, p.size);
            p.opacity -= 0.02;
            p.x = p.x + p.xSpeed;
            p.y = p.y + p.ySpeed;
            p.size *= 0.99
          } else {
            explosionParticles.splice(i, 1);
            i++;
          }
        }
      }

      ctx.globalAlpha = 1;
      ctx.fillStyle = "black"; //['stand', 'jump', 'fall', 'run0', 'run1'];
      if (player.alive) {
        if (player.vX >= 0) {
          if (player.j && player.vY < 1) {
            ctx.drawImage(playerR[1], player.x-10, player.y-16);
          } else if (player.j) {
            ctx.drawImage(playerR[2], player.x-10, player.y-16);
          } else if (player.vX > 0.1 && running < 8) {
            ctx.drawImage(playerR[3], player.x-10, player.y-16);
            running += 1;
          } else if (player.vX > 0.1 && running < 16) {
            ctx.drawImage(playerR[4], player.x-10, player.y-16);
            running += 1;
          } else if (player.vX > 0.1 && running >= 16) {
            ctx.drawImage(playerR[4], player.x-10, player.y-16);
            running = 0;
          } else {
            ctx.drawImage(playerR[0], player.x-10, player.y-16);
          }
        } else {
          if (player.j && player.vY < 1) {
            ctx.drawImage(playerL[1], player.x-10, player.y-16);
          } else if (player.j) {
            ctx.drawImage(playerL[2], player.x-10, player.y-16);
          } else if (player.vX < -0.1 && running < 8) {
            ctx.drawImage(playerL[3], player.x-10, player.y-16);
            running += 1;
          } else if (player.vX < -0.1 && running < 16) {
            ctx.drawImage(playerL[4], player.x-10, player.y-16);
            running += 1;
          } else if (player.vX < -0.1 && running >= 16) {
            ctx.drawImage(playerL[4], player.x-10, player.y-16);
            running = 0;
          } else {
            ctx.drawImage(playerL[0], player.x-10, player.y-16);
          }
        }
      } else {
        if (fallen) {
          ctx.drawImage(fallDeath, 0, 0);
        } else if (ghostKill) {
          ctx.drawImage(ghostDeath, 0, 0);
        } else {
          ctx.clearRect(0, 0, width, height);
        }
      }
    }
  };


  /////////////////////
  // Collision Check //
  /////////////////////

  function colCheck(shapeA, shapeB) {
    var vexX = (shapeA.x + (shapeA.w / 2)) - (shapeB.x + (shapeB.w / 2))
    var vexY = (shapeA.y + (shapeA.h / 2)) - (shapeB.y + (shapeB.h / 2))
    var hWidths = (shapeA.w / 2) + (shapeB.w / 2)
    var hHeights = (shapeA.h / 2) + (shapeB.h / 2)
    var colDir = null;

    if (Math.abs(vexX) < hWidths && Math.abs(vexY) < hHeights) {
      var oX = hWidths - Math.abs(vexX); 
      var oY = hHeights - Math.abs(vexY);         
      if (shapeB.alive) {
        if (vexY < 2) {
          colDir = "b"
        } else {
          if (vexX > 0) {
            colDir = "l";
            shapeA.x += oX;
          } else {
            colDir = "r";
            shapeA.x -= oX;
          }
        }
      } else if (shapeB.colour) {
        if (oX >= oY) {
          if (vexY > 0) {
            colDir = "t";
            // shapeA.y += oY;
          } else {
            colDir = "b";
            shapeA.y -= oY;
          } 
        }
      } else if (oX >= oY) {
        if (vexY > 0) {
          colDir = "t";
          shapeA.y += oY;
        } else {
          colDir = "b";
          shapeA.y -= oY;
        } 
      } else {
        if (vexX > 0) {
          colDir = "l";
          shapeA.x += oX;
        } else {
          colDir = "r";
          shapeA.x -= oX;
        }
      }
    }
    return colDir;
  }


  /////////////
  // Colours //
  /////////////

  function getColour(colour) {
    var hex;
    switch(colour)
    {
    case "red":
      hex = "#d85341";
      break;
    case "yellow":
      hex = "#fff048";
      break;
    case "blue":
      hex = "#387db4";
      break;
    case "orange":
      hex = "#ef8a35";
      break;
    case "green":
      hex = "#54b046";
      break;
    case "purple":
      hex = "#be8be6";
      break;
    case "white":
      hex = "#fff";
      break;
    case "mono":
      hex = "#333";
      break;
    default:
      hex = "#333";
    }
    return hex;
  }
}


/////////////////////
// Event Listeners //
/////////////////////

document.body.addEventListener("keydown", function(e) {
  keys[e.keyCode] = true;
});

document.body.addEventListener("keyup", function(e) {
  keys[e.keyCode] = false;
});

window.addEventListener("load", function(){
  bmusic.play();
  bmusic.loop = true;
  var title = new Image();
  title.src = 'img/meta/title.png';
  title.onload = function() {
    ctx.drawImage(title, 0, 0, width, height);
    window.setTimeout(canvasApp,5000);
  }
});

